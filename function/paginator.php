<?php
/*
Classe Paginator, utilizzata per effettuare l'impaginazione di un array.
PARAMETRI OBBLIGATORI:
page_num->numero della pagina corrente
page_size->numero di record da mostrare per ogni pagina
element->array di elementi su cui effettuare l'impaginazione
VARIABILI RESTITUITE
elementPaginated->risultato della paginazione dell'array element
error->errori che si sono verificati durante l'impaginazione.
*/
class Paginator{
  public $itemsForPage;
  public $currentPage;
  public $prevPage;
  public $nextPage;
  public $element;
  public $error;
  public $elementPaginated;

  //Constructor
  public function __construct($page_num, $page_size, $element){
    //setta valori di default
    $this->itemsForPage = $page_size;
    $this->currentPage = $page_num;
    $this->prevPage = null;
    $this->nextPage = null;
    $this->element = $element;
    $this->error = null;
    $this->elementPaginated = null;

  }

  //paginator main function
  public function paginate(){
    //get page numbers
    $this->_getPageNumbers();
    $this->_getArray();
  }


  //imposta il numero della pagina precedente e della pagina successiva
  private function _getPageNumbers(){
    if($this->currentPage > 0){
      $this->prevPage = $this->currentPage - 1;
    }
    $total_pages = ceil(count($this->element) / $this->itemsForPage);
    if($this->currentPage < $total_pages - 1){
      $this->nextPage = $this->currentPage + 1;
    }
  }

  //setta l'array impaginato
  private function _getArray(){
    $total_items = count($this->element); // numero totale di elementi da inpaginare
    $total_pages = ceil($total_items / $this->itemsForPage); //numero totale di pagine
    if($this->currentPage > $total_pages - 1){
      $this->error = "Numero di pagina richiesto non valido";
    }else{
      //$offset = ($this->currentPage - 1) * $this->itemsForPage; // offset
      $offset = ($this->currentPage) * $this->itemsForPage; // offset
      $this->elementPaginated = array_splice($this->element, $offset, $this->itemsForPage); // splice them according to offset and limit
    }
  }

  //restituisce l'errore se presente
  public function getError(){
    return $this->error;
  }

  //restituisce l'array impaginato
  public function getFinal(){
    return $this->elementPaginated;
  }

  //funzione che restituisce il numero di pagina precedente
  public function getPrevPage(){
    return $this->prevPage;
  }

  //funzione che restituisce il numero di pagina precedente
  public function getNextPage(){
    return $this->nextPage;
  }



}
