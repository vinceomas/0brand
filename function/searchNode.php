<?php
function searchNode($idNode, $language, $search_keyword, $conn){

  $result['nodes'] = null;
  $result['error'] = null;

  try {

    //controllo la presenza del nodo all'interno della tabella node_tree
    $qry = "SELECT idNode
    FROM node_tree
    WHERE idNode = ? LIMIT 1";
    $preStmt = $conn->prepare($qry);
    $preStmt->execute( array(
      $idNode
    ) );
    if($preStmt->rowCount() == 1){
      //il nodo specificato esiste
      //creo la query di ricerca in base alla presenza o meno della variabile search_keyword
      if($search_keyword == null){
        $qry = "SELECT node_tree.idNode, NodeName
        FROM node_tree
        INNER JOIN node_tree_names ON node_tree.idNode = node_tree_names.idNode
        WHERE iLeft > (SELECT iLeft FROM node_tree WHERE idNode = ?) AND iRight < (SELECT iRight FROM node_tree WHERE idNode = ?) AND language = ?";
        $stmt = $conn->prepare($qry);
        $stmt->execute( array(
          $idNode,
          $idNode,
          $language
        ) );
      }else{
        $qry = "SELECT node_tree.idNode, NodeName
        FROM node_tree
        INNER JOIN node_tree_names ON node_tree.idNode = node_tree_names.idNode
        WHERE iLeft > (SELECT iLeft FROM node_tree WHERE idNode = ?) AND iRight < (SELECT iRight FROM node_tree WHERE idNode = ?) AND language = ? AND NodeName LIKE ?";
        $stmt = $conn->prepare($qry);
        $stmt->execute( array(
          $idNode,
          $idNode,
          $language,
          '%' . $search_keyword . '%'
        ) );
      }

      //ottengo l'array associativo risultante
      $nodes = $stmt->fetchAll(\PDO::FETCH_ASSOC);

      //per ogni nodo trovato effettuo il conteggio dei nodi figlio se presenti
      if($stmt->rowCount() > 0){
        for($i = 0; $i < $stmt->rowCount(); $i++){
          $qry = "SELECT idNode
          FROM node_tree
          WHERE iLeft > (SELECT iLeft FROM node_tree WHERE idNode = ?) AND iRight < (SELECT iRight FROM node_tree WHERE idNode = ?) AND level = (SELECT level FROM node_tree WHERE idNode = ?) + 1";
          $stmt2 = $conn->prepare($qry);
          $stmt2->execute( array(
            $nodes[$i]['idNode'],
            $nodes[$i]['idNode'],
            $nodes[$i]['idNode']
          ) );
          $countChild = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
          $nodes[$i]['count_child'] = $stmt2->rowCount();
        }
      }
      $result['nodes'] = $nodes;

    }else{
      //il nodo specificato non esiste
      $result['error'] = "ID nodo non valido";
    }




  } catch(PDOException $e) {
    $result['error'] = $e->getMessage();
  }

  return $result;

}
