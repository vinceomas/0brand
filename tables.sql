--
-- Struttura della tabella `node_tree`
--

CREATE TABLE `node_tree` (
  `idNode` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `level` int(11) NOT NULL,
  `iLeft` int(11) NOT NULL,
  `iRight` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



--
-- Struttura della tabella `node_tree_names`
--

CREATE TABLE `node_tree_names` (
  `idNode` int(11) NOT NULL,
  `language` varchar(250) NOT NULL,
  `NodeName` varchar(250) NOT NULL,
  CONSTRAINT `node_tree_names_ibfk_1` FOREIGN KEY (`idNode`) REFERENCES `node_tree` (`idNode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
