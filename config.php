<?php
// **********************************************************************
//  CONNESSIONE AL DATABASE
// **********************************************************************

	$DB_serverPosition 				= "127.0.0.1";
	$DB_serverPort						= "3306";
	$DB_databaseName					= "0brand";
	$DB_databasePwd						= "";
	$DB_databaseUserName			= "root";

// **********************************************************************



// **********************************************************************
//  CONFIGURAZIONE API
// **********************************************************************

	$DEFAULT_pageSize 				=  100; //valore di default da utilizzare nel caso in cui il parametro page_size non venga passato via GET
	$DEFAULT_pageNum					=  0; 	//valore di default da utilizzare nel caso in cui il parametro page_num non venga passato via GET
	$DEFAULT_Min_pageSize			= 0;		// valore minimo del parametro page_size
	$DEFAULT_Max_pageSize			= 1001;	// valore massimo del parametro page_size

// **********************************************************************
