<?php

include_once("config.php");
include_once("function/searchNode.php");
include_once("function/paginator.php");

$error=null;
//controllo se i parametri obbligato idNode e language sono stati passati via GET, se non sono stati passati ritorna un errore
if(isset($_GET['idNode']) && is_int( intval($_GET['idNode']) ) && $_GET['idNode']>=0 && $_GET['idNode'] != '' && isset($_GET['language']) && $_GET['language'] != '' && (strtolower($_GET['language'] == 'english') || strtolower($_GET['language']) == 'italian')){
  $idNode = filter_var($_GET['idNode'], FILTER_VALIDATE_INT);
  $language = filter_var($_GET['language'], FILTER_SANITIZE_STRING);

  //controllo se è settato il parametro page_size, se non è impostato lo imposto di default a 100, se è maggiore di 1000 ritorno un errore.
  if(!isset($_GET['page_size'])){
    $page_size = $DEFAULT_pageSize;
  }else{
    if($_GET['page_size'] > $DEFAULT_Min_pageSize && $_GET['page_size'] < $DEFAULT_Max_pageSize){
      $page_size = filter_var($_GET['page_size'], FILTER_VALIDATE_INT);
    }else{
      $error = "Richiesta formato pagina non valido";
    }
  }
  //controllo se è settato il parametro page_num, se non è settato lo imposto di default a 0
  if(isset($_GET['page_num'])){
    if(is_int(intval($_GET['page_num'])) && $_GET['page_num'] >= 0){
      $page_num = filter_var($_GET['page_num'], FILTER_VALIDATE_INT);
    }else{
      $error = "Numero di pagina richiesto non valido";
    }
  }else{
    $page_num = $DEFAULT_pageNum;
  }

  //controllo se è settato il parametro search_keyword, se non è settato lo imposto a null
  if(isset($_GET['search_keyword'])){
    $search_keyword = filter_var($_GET['search_keyword'], FILTER_SANITIZE_STRING);
  }else{
    $search_keyword = null;
  }

  //connessione al database
  try{
    $conn = new PDO("mysql:host=$DB_serverPosition;dbname=$DB_databaseName", $DB_databaseUserName, $DB_databasePwd);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }catch(\Exception $e){
    $error = $e->getMessage();
  }

  if($error == null){
    //effettuo la ricerca passando i parametri alla funzione searchNode
    $nodes = searchNode($idNode, $language, $search_Keyword, $conn);

    //se la ricerca a restituito risultati effettuo la paginazione, altrimenti stampo l'errore ricevuto dalla funzione searchNode
    if($nodes['nodes'] != null && $nodes['error'] == null){
      $paginator = new Paginator($page_num, $page_size, $nodes['nodes']);
      $paginator->paginate();
      $nodesPaginated = $paginator->getFinal();
      $nextPage = $paginator->getNextPage();
      $prevPage = $paginator->getPrevPage();

      if($paginator->getError() != null){
        $error = $paginator->getError();
      }

    }else{
      $error = $nodes['error'];
    }
  }





}else{
  $error = "Parametri obbligatori mancanti";
}

$result['nodes'] = $nodesPaginated;
$result['nextPage'] = $nextPage;
$result['prevPage'] = $prevPage;
$result['error'] = $error;
$json_output = json_encode($result);
header('Content-Type: application/json');
echo $json_output;


?>
